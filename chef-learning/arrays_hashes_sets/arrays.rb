months = ["January", "February", "March", "April", "May", "June", "July"]

puts "Eval: months[6]"
puts months[6] 

# add items to array
months << "August"

puts "Eval: puts months (after adding August with: months << 'August')"
puts months

# add at specific location
months.insert(2, "October")
puts "Eval: print afer adding 'October' to 2th position"
puts months

# remove last item
months.pop
puts "Eval: print afer months.pop"
puts months

# delete at position
months.delete_at(2)
puts "Eval: print months after removing 2th element"
puts months

arr_2d = [[1,2,3], [4,5,6]]
puts "Eval: print 2d array [[1,2,3],[4,5,6]] for each"
arr_2d.each { |x| puts "#{x}\n" }

puts "Eval: arr_2d[1][1]"
puts arr_2d[1][1]

my_arr = [5, 9, 8,2,6]

puts "my_arr = #{my_arr}"
puts "Eval: my_arr.include?(0)"
puts my_arr.include?(0)
puts "Eval: my_arr.include?(2)"
puts my_arr.include?(2)

puts "Eval: my_arr.sort"
print my_arr.sort
print "\n"

new_arr = [5, 9, [8,2,6], [1, 0]]

puts "new_arr = #{new_arr}"

puts "Eval: new_arr.flatten"
print my_arr.flatten
print "\n"

puts "Eval: my_arr.map { |item| item**2 }"
print my_arr.map { |item| item*2 }
print "\n"
