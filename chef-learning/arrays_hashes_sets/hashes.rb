person_hash = {
  "name" => "Jared",
  "age" => 25
}

puts "Person hash: #{person_hash}"

puts "Eval: person_hash['name']"
puts person_hash['name']

person_hash["gender"] = 'male'
puts "Eval: person_hash['gender'] = 'male'"
puts person_hash

person_hash.delete('gender')
puts "Eval: person_hash.delete('gender')"
puts person_hash

person_hash.each do |key, value|
   puts "#{key} is #{value}"
end

puts 'Eval: person_hash.has_key?("name")'
puts person_hash.has_key?("name")
puts 'Eval: person_hash.has_key?("height")'
puts person_hash.has_key?("height")

puts 'Eval: person_hash.select{ |key, value| key =="name" }'
puts person_hash.select{ |key, value| key =="name" }

puts 'Eval: person_hash.fetch("name")'
puts person_hash.fetch("name")
