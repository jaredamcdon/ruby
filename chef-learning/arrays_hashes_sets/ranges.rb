# prints 1-9
puts "Eval: for count in 1...10"
for count in 1...10
  puts count
end

#prints 1-10
puts "Eval: for count in 1..10"
for count in 1..10
  puts count
end

#prints 1-10
puts "Eval: my_range = Range.new(1,10)"
my_range = Range.new(1,10)
puts "my_range: #{my_range}"
puts "Eval: my_range.each { |i| puts i }"
my_range.each { |i| puts i }
