require 'set'
my_set = Set.new([5, 2, 9, 3, 1])

print my_set
print "\n"
my_set.each do |x|
   puts x
end

puts "Eval: my_set << 7"
my_set << 7
puts my_set

puts "Eval: my_set.add 4"
my_set.add 4
puts my_set
