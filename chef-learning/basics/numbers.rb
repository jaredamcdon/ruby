x = 5.0
y = 3

puts "x = #{x} y = #{y}"

# addition
sum = x + y
puts "sum: #{sum}"

# subtraction
sub = x - y
puts "subtraction: #{sub}"

# multiplication
mul = x * y
puts "multiplication: #{mul}"

# division
div = x / y
puts "division: #{div}"

# modulus
mod = x % y
puts "modulus: #{mod}"
