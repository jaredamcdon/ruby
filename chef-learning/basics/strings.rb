# interpolation
name = "Jared"
age = 21
puts "Your name is #{name} and you are #{age} years old"

# concatenation
str_1 = "Hello"
str_2 = "World"
puts str_1 + str_2

# string manipulation
string = "I love Ruby"
puts "\nstring:\n" + string

puts string.length

puts string.reverse

puts string.downcase

puts string.upcase
