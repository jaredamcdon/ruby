my_var = 5
puts my_var

x = 5
y = 15

sum = x + y
puts sum

# variables are automatically assigned types
name = "Emily"
age = 25
puts "Your name is #{name} and you are #{age} years old"
