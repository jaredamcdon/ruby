num = 0

puts "num = #{num}"

puts "case statement: case num"
case num
when 0
  puts "Eval: when 0"
  puts "zero"
when 1
  puts "Eval: when 1"
  puts "one"
when 2
  puts "Eval: when 2"
  puts "two"
else
  puts "Eval: else"
  puts "number greater than 2"
end
