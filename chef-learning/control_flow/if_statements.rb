x = 10

if x > 7
  puts "Eval statement: if x > 7"
  puts "x (#{x}) is greater than 7\n"
end

if x < 7
  puts "Eval statement: if x < 7"
  puts "x (#{x}) is less than 7\n"
elsif x > 7
  puts "Eval statement: if x < 7, elsif x > 7"
  puts "x (#{x}) is greater than 7\n"
else
  puts "Eval statement: if x < 7, elsif x > 7, else"
  puts "x (#{x}) is equal to 7\n"
end

playing = false
puts "playing = #{playing}"

unless playing
  puts "Eval statement: unless playing"
  puts "We're busy learning Ruby\n"
else
  puts "Eval statement: unless playing, else"
  puts "It's time to play games\n"
end
