test_file = File.new("test.txt", "w+")

# open created file
test_file = File.open("test.txt", "w+")

# write to file
test_file.puts("writing to a file with ruby")
test_file.close

puts File.read("test.txt")

# due to how file is being written, text.txt is overwritten
# write using a code block, does not require .close
File.open("test.txt", "a") {
  |file| file.puts("this text was added via code block")
}

# read a file
puts File.read("test.txt")

puts "Eval: name = gets"
puts "please enter your name"
name = gets

puts "Hi, #{name}"
