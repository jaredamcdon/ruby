require 'webrick'

#specify listening port
http_server = WEBrick::HTTPServer.new(:Port => 3000)

# inherit functionality from WEBrick HTTPServlet
class MyHTTPServlet < WEBrick::HTTPServlet::AbstractServlet
  # outputs requested path
  def do_GET(http_request, http_response)
    http_response.body = "You requested '#{http_request.path}'"
  end
end

http_server.mount('', MyHTTPServlet)
# stop server with CTRL-C
trap('INT') {http_server.shutdown}
http_server.start
=begin
to make a call with curl:
curl localhost:3000
response: You requested '/'
=end
