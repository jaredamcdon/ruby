#initializing classes
class MyClass
end

class Car
  def initialize(brand)
    @brand = brand
    puts "car object created with brand #{brand}"
  end
end

car = Car.new("audi")
puts car

class Greetings
    def self.class_greeting
          puts "hello, im a class method"
            end
      def instance_greeting
            puts "hello, im an instance method"
              end
end

Greetings.class_greeting
# output: hello, im a class method

greeting_instance = Greetings.new

greeting_instance.instance_greeting
# output: hello im an instance method

class Books
    @@count = 0
      def initialize
            @@count += 1
              end
        def self.get_instance_count
              @@count
                end
end

puts Books.get_instance_count
#outputs 0

book1 = Books.new
book2 = Books.new
puts Books.get_instance_count
#outputs 2


module Greet
  def initialize
    puts "you just initialized Greet in your class"
  end
  def say_hello
    puts "Hello!"
  end
end

class Person
  include Greet
end

p1 = Person.new
# output: you just initialized Greet in your function
p1.say_hello
# output: Hello!

class ClassInheritance
  extend Greet
end

ClassInheritance.say_hello
# output: Hello!
