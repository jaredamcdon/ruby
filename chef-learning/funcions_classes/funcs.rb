# make a function
def greetings
  puts "Hello!"
end

greetings

# function with included arg
def greet_with_name(name="Jared")
    puts "Hello #{name}!"
end

greet_with_name
greet_with_name("Alex")

# function with undefined args
def sum(x,y)
    x+y
end

puts sum(2,9)

def put_with_optional_args(*a)
    puts a
end

put_with_optional_args("It", "is", 2021)

# return statements
def prod(x,y)
  res = x*y
  return res
end

puts "Eval: prod(x,y) with return, puts return"
result = prod(3,5)
puts result

# explicit return (nothing after return executes)
def expl_return
  puts 'this is before the return call'
  return 'this is the return call'
  puts 'this will never be executed'
end

puts expl_return

def impl_return_prod(x,y)
  x*y
end

puts impl_return_prod(3,4)

# yields
def yield_test
    puts "in the method"
      yield
        puts "back in the method"
end

yield_test { puts "this is the yield, not in the method, in the block" }

=begin
output:
in the method
this is the yield, not in the method, in the block
back in the method
=end

# yield with params added
def yield_with_params(name)
  puts "in method"
  yield("Emily")
  puts "between yield"
  yield(name)
  puts "in method"
end

yield_with_params("Eric") { |n| puts "Hello #{n}."}
