# Ruby
> Hey we're learning ruby today, come along for the ride

### Need packages? Look no further
[ruby-toolbox.com/](https://www.ruby-toolbox.com/)

### __quick links__
|   |   |   |   |
|---|---|---|---|
| [where](#where) | [running ruby](#running-ruby) | [package management](#package-management) | [reserved words](#reserved-words) |
| [logical operators](#logical-operators) | [overview](#overview) | [general guidelines](#general-guidlines)
| [syntax](#syntax) | [puts, print, gets](#puts,-print,-gets) | [conditionals](#conditional-syntax) | [functions](#functions)
| [classes](#classes)| [for loops](#for-loops) | [code blocks](#code-blocks) | [variables](#variables) 
| [comments](#comments) | [arrays](#arrays) | [hashes](#hashes) | [sets](#sets) |
| [file I/O](#file-I/O) | [HTTP](#HTTP) | [tests](#tests)

### where
 - [chef.io](https://learn.chef.io/courses/course-v1:chef+Ruby101+Perpetual/)

### running ruby
 - `ruby file.rb`

### package management
 - `gem install [package]`

## reserved words

|   |   |   |   |   |
|---|---|---|---|---|
| def | if | else | elsif | do |
| next | then | retry | while | class |
| rescue | when | in | self | module |
| until | redo | return | case | or |
| begin | end | super | unless | false |
|break | for | defined? | alias | and |

## logical operators

|   |   |
|---|---|
| && (and) | if both true |
| \|\| (or) | if any true | 
| ! (not) | reverse operand |

## an overview

 - dynamically typed
 - no semicolons
 - supports template literals
 - weakly typed
 - can be compiled

## general guidlines

### syntax
 - Ruby is not particular about whitespace
   - `var = 5+5` == `var = 5 + 5`
 - Might not allow vars to start with numbers?
 - vars_should_be_snake_case
 - ClassesArePascalCase

### puts, print, gets
 - puts
   - works like python `print()`
 - print
   - does not `\n`
 - gets
   - read from console
   - `var = gets`

### conditional syntax

```ruby
if x > y
  puts "x > y"
elsif x < y
  puts "x < y"
else
  puts "x == y"
end
```

### functions
 - uses `def`
```ruby
def greeting
  puts "Hello!"
end
```
 - calling classes
   - `greetings`
 - passing vars
```ruby
def greet_with_name(name="Jared")
  puts "Hello #{name}!"
end

greet_with_name
#output = Hello Jared!

greet_with_name("Joe")
#output = Hello Joe!

def sum(x,y)
  x+y
end

puts sum(2,9)
#output = 11

def put_with_optional_args(*a)
  puts a
end

put_with_optional_args("It", "is", 2021)
=begin
output:
It
is
2021
=end
```
 - return values
   - `return var`
 - explicit returns 
   - all code after return will not be executed
 - implicit returns
   - if no return statement last executed instruction is returned
### classes
>Ruby is object oriented so everything is objects defined by classes
 
 - initializing
```ruby
class MyClass
end
```
 - instance methods and class methods
   - instance methods
     - `instance_method_name`
     - only callable by an instance (object derived from class)
   - class methods
     - `self.method_name`
     - only callable by the class (cannot be derived from an instantiated object)
```ruby
class Greetings
  def self.class_greeting
    puts "hello, im a class method"
  end
  def instance_greeting
    puts "hello, im an instance method"
  end
end

Greetings.class_greeting
# output: hello, im a class method
Greetings.instance_greeting
# outputs error

greeting_instance = Greetings.new

greeting_instance.instance_greeting
# output: hello im an instance method
greeting_instance.class_greeting
#outputs error
``` 
 - class variables
    - how you create variables __inside__ a class for class usage __only__
    - `@@count = 0`
```ruby
class Books
  @@count = 0
  def initialize
    @@count += 1
  end
  def self.get_instance_count
    @@count
  end
end

puts Books.get_instance_count
#outputs 0

book1 = Books.new
book2 = Books.new
puts Books.get_instance_count
#outputs 2
```
 - initialize method
   - `def initialize`
   - called when object is created
 - instance variables
   - how you pass values into class
   - `@var = var`
```ruby
class Car
  def initialize(brand)
    @brand = brand
  end
end

audi_car = Car.new("audi")
```

 - mixins
   - inheriting classes
     - include
       - `include ModuleName`
       - grants access to instance methods
     - extend
       - `extend ModuleName`
       - grants access to class methods
```ruby
module Greet
  def initialize
    puts "you just initialized Greet in your class"
  end
  def say_hello
    puts "Hello!"
  end
end

class Person
  include Greet
end

p1 = Person.new
# output: you just initialized Greet in your class
p1.say_hello
# output: Hello!

def ClassInheritance
  extend Greet
end

ClassInheritance.say_hello
# output: Hello!
```

### for loops

 - times and each

```ruby
# prints 5 times
5.times { puts "I am Learning Ruby!" }

#prints each day of the week
days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']

days.each { |day| puts day }

```
 - ranges
```ruby
# prints 1-9
for count in 1...10
  puts count
end

#prints 1-10
for count in 1..10
  puts count
end

#prints 1-10
my_range = Range.new(1,10)
my_range.each { |i| puts i }
```

### code blocks
 - single line code blocks
```ruby
if x == true { puts "x is true" }
```
 - multiline code blocks
```ruby
if x == true
  puts "x is true"
end
```
 - yield
   - yield allows you to call method outside the block
```ruby
def yield_test
  puts "in the method"
  yield
  puts "back in the method"
end

yield_test { puts "this is the yield, not in the method, in the block" }

=begin
output:
in the method
this is the yield, not in the method, in the block
back in the method
=end

def yield_with_params(name)
  puts "in method"
  yield("Emily")
  puts "between yield"
  yield(name)
  puts "in method"
end

yield_with_params("Eric") { |n| puts "Hello #{n}."}

=begin
output:
in method
Hello Emily.
between yield
Hello Eric.
in method
=end
```

## variables
> Ruby is case sensitive and so are identifiers

Proper naming convention
 - snake_case
 - lower_case
 - example:
   - `my_variable`

### comments
 - `#single line comment`
```ruby
=begin
multi-line
comment
=end
```

## storing values in objects

### arrays
 - initializing
   - `months = []`
   - `months = ['January', 'February']`
 - adding
    - `months << 'March'`
    - `months.insert(3, 'April')`
 - deleting
    - `months.pop` #_last position_
    - `months.delete_at(2)`
 - 2d arrays
    - `arr_2d = [[1,2,3], [4,5,6]]`
 - calling elements
    - `months[3]`
    - `arr_2d[1][1]`
 - include?
    - `months.include?("January")` #_prints true_
 - sort
```ruby
my_arr = [5,3,7,4]
my_arr.sort #output = [3,4,5,7]
```
 - flatten 
```ruby
partial_2d_array = [5, 9, [8, 2, 6], [1, 0]]
partial_2d_array.flatten #output = [5,9,8,2,6,1,0]
```
 - map
   - replaces array with new array after invoking method
```ruby
my_array = [5, 9, 8, 2, 6]
print my_array.map { |item| item*2}
# results in my_array = [10, 18, 16, 4, 12]
```

### hashes
 - initializing
   - `jar_hash = Hash.new`
```ruby
jar_hash = {
  "name" => "Jared",
  "age" => 21
}
```
 - adding
   - `jar_hash["gender"] = "male"`
 - deleting
   - `jar_hash.delete('gender')`
 - iterating
```ruby
person_hash.each do |key, value|
 puts "#{key} is #{value}"
end
```
 - has_key?
   - `jar_hash.has_key?("name") # evaluates as true`
 - select
   - `jar_hash.select{ |key, value| key == "name"}` _#returns key value pair that matches key name "name"_
 - fetch
   - `jar_hash.fetch("name")` _#returns value of key "name"_

### sets
> sets are like arrays where __1) order doesnt matter__ and __2) all elements unique__
 - printing does NOT work well with sets, can be worked around, see _printing_
 - intializing
```ruby
require 'set'
# initialize empty
my_set = Set.new
# intialize with array
my_set = Set.new([1,4,8,3])
```
 - printing
```ruby
print my_set
#output: #<Set: {5, 2, 9, 3, 1}> 

my_set.each do |x|
  puts x
end
=begin
output:
5
2
9
3
1
=end
```

 - adding
```ruby
my_set << 7
#output: #<Set: {5, 2, 9, 3, 1, 7}>
my_set.add 4
#output: #<Set: {5, 2, 9, 3, 1, 7, 4}>
```

## File I/O
> read and write files in ruby
### File operators
| syntax | usage |
|---|---|
| r | default mode for files in ruby, read-only access, starts reading file at beginning |
| r+ | write only access, starting reading at beginning of file |
| w | write-only access, truncates existing file and creates new file |
| w+ | read and write access, truncates existing file and overwrites for reading and writing |
| a | write only, appends to end of file |
| a+ | read and write access, appends or reads from end of file |

### command usage
```ruby
# create file with read/write access
test_file = File.new("test.txt", "w+")

# open created file
test_file = File.open("test.txt", "w+")

# write to file
test_file.puts("writing to a file with ruby")
test_file.close

puts File.read("test.txt")
# output: writing to a file with ruby

# write using a code block, does not require .close
File.open("test.txt", "a") {
  |file| file.puts("this text was added via code block")
}

# read a file
puts File.read("test.txt")
=begin
output:
writing to a file with ruby
this text was added via code block
=end
```

## HTTP
> Ruby comes built in with an HTTLP client `net/http`

 - http response calling
```ruby
require 'net/http'
http_response = Net::HTTP.get_response('www.duckduckgo.com', '/')
puts http_response.code
# output: 200/301/400 etc
puts http_response.body
# outputs html in plaintext
```
 - parsing
```ruby
require 'net/http'
require 'uri'
require 'json'
uri = URI('https://api.coingecko.com/api/v3/simple/price?ids=cardano%2Cbitcoin&vs_currencies=usd')
response = Net::HTTP.get(uri)
json_response = JSON(response)

puts json_response
# example output: {"bitcoin"=>{"usd"=>55836}, "cardano"=>{"usd"=>1.25}}
```

 - http serving
```ruby
require 'webrick'

#specify listening port
http_server = WEBrick::HTTPServer.new(:Port => 3000)

# inherit functionality from WEBrick HTTPServlet
class MyHTTPServlet < WEBrick::HTTPServlet::AbstractServlet
  # outputs requested path
  def do_GET(http_request, http_response)
    http_response.body = "You requested '#{http_request.path}'"
  end
end

http_server.mount('', MyHTTPServlet)
# stop server with CTRL-C
trap('INT') {http_server.shutdown}
http_server.start

=begin
to make a call with curl:
curl localhost:3000
# response: You requested '/'
=end
```

 - Ruby Toolbox
   - way more options and documentation
   - [ruby-toolbox.com/](https://www.ruby-toolbox.com/)

## tests

 - testing your code can be done in line with your code
 - example:
```ruby
require "test/unit/assertions"

include Test::Unit::Assertions

hello = "Hello World!"

# if passes, nothing occurs
# if fails, error will print
assert_equal 'Hello World!', hello, "function should return 'Hello World!'"
```
### assertion functions

| | | | |
|---|---|---|---|
| assert | assert_block | assert_equal | assert_fail_assertion |
| assert_boolean | assert_compare | assert_empty | assert_false |
| assert_in_delta | assert_in_epsilon | assert_const_defined | assert_include |
| assert_instance_of | assert_kind_of | assert_match | assert_nil |
| assert_no_match | assert_not_const_defined | assert_raise | assert_same | assert_same |
| assert_not_empty | assert_throws | assert_raises | assert_operator |
| assert_not_equal | assert_not_in_delta | assert_not_in_epsilon | flunk |

---

#### Test::Unit is not the only testing framework in ruby
 - assert is the most basic assertion function
 -  another example
```ruby
require "test/unit/assertions"

include Test::Unit::Assertions
pass = true
assert pass, "Test should pass"
```
### RSpec testing
 - `gem install rspec`
 - rspec --init
   - output:
```sh
create .rspec
create spec/spec_helper.rb
```
  - put tests in spec/
  - save tests as `file_name_spec`
  - run tests as `rspec spec/file_name_spec.rb`
  - Example:
    - hello.rb:
```ruby
class Greetings
 def self.say_hello

  'Hello!'
 end
end
```
 - spec/hello_spec.rb
```ruby
require_relative '../hello'

RSpec.describe Greetings do
 context "#greetings" do
  it { expect(Greetings.say_hello).to eql 'Hello!' }
 end
end
```
 - Output:
```sh
jared@localhost:~$ rspec spec/hello_spec.rb
.

Finished in 0.03217 seconds (files took 0.32109 seconds to load)
1 example, 0 failures
```

### minitest

#### minitest features
| require statement | usage |
|---|---|
| minitest/autorun | explicitly run all tests |
| minitest/test | test system |
| minitest/spec | spec system |
| minitest/mock | mock/stub system |
| minitest/benchmark | benchmark algorithm performance |
| minitest/pride | pride in testing? |

 - another testing library
 - compatible with Test::Unit & Rspec
 - Example using hello.rb (above)
 - hello_test.rb:
```ruby
require 'minitest/autorun'
require_relative './hello'

class GreetingsTest < Minitest::Test
 def test_greetings
   assert_equal 'Hello!', Greetings.say_hello, "Greetings.say_hello should return 'Hello!'"
 end
end
```
 - Output:
```sh
jared@localhost:~$ ruby hello_test.rb
Run options: --seed 41476

# Running:

.

Finished in 0.007111s, 140.6351 runs/s, 140.6351 assertions/s.

1 runs, 1 assertions, 0 failures, 0 errors, 0 skips
```
 - a spec & minitest example:
   - using hello.rb (above)
   - hello_spec.rb:
```ruby
require 'minitest/autorun'
require_relative './hello'

describe Greetings do
 describe "#sayhello" do
  it "should return Hello!" do
   _(Greetings.say_hello).must_equal 'Hello!'
  end
 end
end
```
 - Output:
```sh
jared@localhost:~$ ruby hello_spec.rb
Run options: --seed 5837

# Running:

.

Finished in 0.006280s, 159.2331 runs/s, 159.2331 assertions/s.

1 runs, 1 assertions, 0 failures, 0 errors, 0 skips
```
