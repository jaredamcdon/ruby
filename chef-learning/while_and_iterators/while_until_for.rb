
# while
count = 1

puts "count = #{count}"

puts "Eval: while count < 10"
while count < 10
  puts count
  count = count + 1
end

# until
count = 6

puts "count = #{count}"

puts "Eval: until count > 10"

until count > 10
  puts count
  count = count + 1
end

# for
puts "Eval: for count in 1...10"

for count in 1...10
  puts count
end
