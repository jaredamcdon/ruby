# Lets put Ruby on some Rails!
> first app so I can learn dockerization and rails basics

- This was built using: 
```
╰─ ruby --version
ruby 2.7.0p0 (2019-12-25 revision 647ee6f091) [x86_64-linux-gnu]
╰─ rails --version
Rails 6.1.3.1
╰─ node --version
v10.19.0
╰─ yarn --version
1.22.10

additional packages used:
  - webpacker 5.0
  - via gemfile > gem 'webpacker', '~> 5.0'
```

- This was based upon the:
  - [Rails 6, Bootstrap 5: a tutorial](https://bootrails.com/blog/rails-bootstrap-tutorial)
- Having issues? I sure did. try:
  - `yarn add @rails/webpacker@next`
  - `bundle exec rails webpacker:install`

## some important commands

|   |   |
|---|---|
| rails s | start webserver |
| rails assets:clobber | updated assets files |
| rails webpacker:compile | compile webpacker |
| rails g model __modelName__ *attributes | create a model *make model name singular |
| rails g controller __modelName__s index show new edit | generate a models controller *plural |


## The important files and structure of RoR

- __Gemfile__ #_This file was modified to add ruby packages_
- Gemfile.lock 
- README.md 
- Rakefile 
- babel.config.js 
- config.ru 
- package.json 
- postcss.config.js 
- yarn.lock

- app:
  - assets
  - channels
  - controllers
    - application_controller.rb
    - concerns
    - __welcome_controller.rb__ #_this was created in the guide_
  - helpers
  - __frontend__ #_this was javascript/ but changed due to guide_
    - packs
      - __application.js__ #_modified_
      - __application.scss__ #_created_
  - jobs
  - mailers
  - models
  - views
    - layouts
    - __welcome__ #_this directory/files create in guide_
      - __index.html.erb__
- config:
  - application.rb
  - cable.yml
  - database.yml
  - environments
  - locales
  - __routes.rb__ #_this is all your routing_
  - storage.yml
  - webpacker.yml
  - boot.rb
  - credentials.yml.enc
  - environment.rb
  - initializers
  - puma.rb
  - spring.rb
  - webpack

- public:
  - 404.html
  - 422.html
  - 500.html
  - apple-touch-icon-precomposed.png
  - apple-touch-icon.png
  - favicon.ico
  - robots.txt
