=begin
Runtime: 52 ms, faster than 96.34% of Ruby online submissions
Memory Usage: 210.6 MB, less than 49.39% of Ruby online submissions
/---/
Given the array nums consisting of 2n elements in the form [x1,x2,...,xn,y1,y2,...,yn].
Return the array in the form [x1,y1,x2,y2,...,xn,yn].
=end
# @param {Integer[]} nums
# @param {Integer} n
# @return {Integer[]}
def shuffle(nums, n)
    out = []
    for i in 0...(nums.size/2)
        out << nums[i]
        out << nums[i+n]
    end
    return out
end
