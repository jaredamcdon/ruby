=begin
Runtime: 48 ms, faster than 70.00% of Ruby online submissions
Memory Usage: 209.9 MB, less than 20.00% of Ruby online submissions
####
Given a positive integer num, output its complement number. The complement strategy is to flip the bits of its binary representation.
####
Example 1:

Input: num = 5
Output: 2
Explanation: The binary representation of 5 is 101 (no leading zero bits), and its complement is 010. So you need to output 2.
=end
# @param {Integer} num
# @return {Integer}
def find_complement(num)
    replace = {
        '1' => '0',
        '0' => '1'
    }
    bin = num.to_s(2)
    rev_bin = bin.gsub(Regexp.union(replace.keys), replace)
    rev_bin.to_i(2)
end
