=begin
Hercy wants to save money for his first car. He puts money in the Leetcode bank every day.

He starts by putting in $1 on Monday, the first day. Every day from Tuesday to Sunday, he will put in $1 more than the day before. On every subsequent Monday, he will put in $1 more than the previous Monday.

Given n, return the total amount of money he will have in the Leetcode bank at the end of the nth day.
#####
Runtime: 40 ms, faster than 100.00% of Ruby online submissions
Memory Usage: 209.8 MB, less than 55.00% of Ruby online submissions
=end
# @param {Integer} n
# @return {Integer}
def total_money(n)
    @n = n
    rem_days = n % 7
    week =  n / 7

    week_day_1 = week + 1
    money = 0
    if week > 0
        for i in 0...week
            if i == 0
                money = 28
                last = 28
            else
                money+= last + 7
                last = last + 7
            end
        end
    end
    
    day_arr = [0,1,2,3,4,5,6]
    for i in 0...rem_days
        money = money + week_day_1 + day_arr[i]
    end
    return money
end

puts "Enter amount of days saving"
days = gets

puts total_money(days.to_i)
