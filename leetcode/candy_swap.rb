# @param {Integer[]} a
# @param {Integer[]} b
# @return {Integer[]}
def fair_candy_swap(a, b)
    move = (a.sum - b.sum) /2
    a = a.uniq
    b = b.uniq
    if move < 0
        move = move.abs
        for i in 0...b.size
            if b[i] >=move
                target = b[i] - move
                for x in 0...a.size
                    if target == a[x]
                        return [a[x],b[i]]
                    end
                end
            end
        end
    else
        for i in 0...a.size
            if a[i] >= move
                target = a[i] - move
                for x in 0...b.size
                    if target == b[x]
                        return [a[i],b[x]]
                    end
                end
            end
        end
    end
end

puts fair_candy_swap([1,1],[2,2])
