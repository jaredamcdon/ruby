def get_change(input)
  input = (input * 100).to_i % 100
  change = 0
  coins_val = [25,10,5,1]
  coins = [0,0,0,0]
  until input == 0
    if input >= coins_val[0]
      change += coins_val[0]
      input -= coins_val[0]
      coins[coins_val.size-1] = coins[coins_val.size-1] + 1
    else
      coins_val.delete_at(0)
    end
  end
  return coins
end
puts "input: "
inp = gets.to_f
coins = get_change(inp)

puts "Quarters:\t#{coins[3]}\nDimes:\t\t#{coins[2]}\nNickels:\t#{coins[1]}\nPennies:\t#{coins[0]}"
