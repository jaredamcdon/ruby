=begin
Runtime: 60 ms, faster than 69.32% of Ruby online submissions
Memory Usage: 210.2 MB, less than 35.23% of Ruby online submissions
/---/
A pangram is a sentence where every letter of the English alphabet appears at least once.
Given a string sentence containing only lowercase English letters, return true if sentence is a pangram, or false otherwise.
=end
# @param {String} sentence
# @return {Boolean}
def check_if_pangram(sentence)
    alpha = [
        "a","b","c","d",
        "e","f","g","h",
        "i","j","k","l",
        "m","n","o","p",
        "q","r","s","t",
        "u","v","w","x",
        "y","z"
    ]
    sentence = sentence.split('')
    for char in sentence
        if alpha.include? char
            alpha.delete(char)
        end 
    end
    if alpha == []
        return true
    else
        return false
    end
end
