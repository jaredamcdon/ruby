=begin
Runtime: 60 ms, faster than 60.98% of Ruby online submissions
Memory Usage: 211 MB, less than 73.17% of Ruby online submissions
=end

# @param {Integer[]} target
# @param {Integer[]} arr
# @return {Boolean}
def can_be_equal(target, arr)
    arr.sort == target.sort
end
