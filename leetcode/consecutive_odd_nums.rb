=begin
Runtime: 48 ms, faster than 86.67% of Ruby online submissions
Memory Usage: 209.9 MB, less than 73.33% of Ruby online submissions
/---/
Given an integer array arr, return true if there are three consecutive odd numbers in the array. Otherwise, return false. 
=end
# @param {Integer[]} arr
# @return {Boolean}
def three_consecutive_odds(arr)
    counter = 0
    keepgoing = true
    for i in 0...arr.size
        if arr[i].odd?
            counter += 1
        else
            counter = 0
        end
        if counter == 3
            return true
        end
    end
    return false
end
