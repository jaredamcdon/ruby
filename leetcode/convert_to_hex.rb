=begin
Runtime: 56 ms, faster than 50.00% of Ruby online submissions
Memory Usage: 209.8 MB, less than 100.00% of Ruby online submissions
##
Given an integer num, return a string representing its hexadecimal representation. For negative integers, two’s complement method is used.

All the letters in the answer string should be lowercase characters, and there should not be any leading zeros in the answer except for the zero itself.
=end

# @param {Integer} num
# @return {String}
def to_hex(num)
    comps = {
        '0' => 'f', '1' => 'e', '2' => 'd', '3' => 'c',
        '4' => 'b', '5' => 'a', '6' => '9', '7' => '8',
        '8' => '7', '9' => '6', 'a' => '5', 'b' => '4',
        'c' => '3', 'd' => '2', 'e' => '1', 'f' => '0'
    }
    if num < 0
      num = num + 1
      hex = num.to_s(16)
      hex = hex.tr('-', '')
      if hex.size < 8
        count = 8 - hex.size
        zeros ='0' * count
        hex = "#{zeros}#{hex}"
      end
      hex.gsub(Regexp.union(comps.keys), comps)
    else
      num.to_s(16)
    end
end
