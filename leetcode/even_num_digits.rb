=begin
Runtime: 52 ms, faster than 86.79% of Ruby online submissions
Memory Usage: 210.2 MB, less than 57.55% of Ruby online submissions
/---/
Given an array nums of integers, return how many of them contain an even number of digits. 
=end
# @param {Integer[]} nums
# @return {Integer}
def find_numbers(nums)
    count = 0
    for i in nums
        if i.to_s.size % 2 == 0
            count+=1
        end
    end
    return count
end
