=begin
Runtime: 48 ms, faster than 83.02% of Ruby online submissions
Memory Usage: 209.8 MB, less than 30.19% of Ruby online submissions
###
A school is trying to take an annual photo of all the students. The students are asked to stand in a single file line in non-decreasing order by height. Let this ordering be represented by the integer array expected where expected[i] is the expected height of the ith student in line.

You are given an integer array heights representing the current order that the students are standing in. Each heights[i] is the height of the ith student in line (0-indexed).

Return the number of indices where heights[i] != expected[i]
=end

# @param {Integer[]} heights
# @return {Integer}
def height_checker(heights)
    @heights = heights
    sorted = heights.sort
    err = 0
    for i in 0...heights.size
        if heights[i] != sorted[i]
            err += 1
        end
    end
    return err
end

puts height_checker([1,2,7,4,1,8])

