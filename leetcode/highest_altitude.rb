=begin
Runtime: 56 ms, faster than 53.45% of Ruby online submissions
Memory Usage: 209.6 MB, less than 87.93% of Ruby online submissions
/---/
There is a biker going on a road trip. The road trip consists of n + 1 points at different altitudes. The biker starts his trip on point 0 with altitude equal 0.
You are given an integer array gain of length n where gain[i] is the net gain in altitude between points i and i + 1 for all (0 <= i < n). Return the highest altitude of a point.
=end
# @param {Integer[]} gain
# @return {Integer}
def largest_altitude(gain)
    out = [0]
    gain.each { |i| out << out[-1] + i }
    return out.max
end
