=begin
Runtime: 56 ms, faster than 54.29% of Ruby online submissions
Memory Usage: 209.9 MB, less than 5.71% of Ruby online submissions
/----/
Given an array of positive integers nums, return the maximum possible sum of an ascending subarray in nums.

A subarray is defined as a contiguous sequence of numbers in an array.

A subarray [numsl, numsl+1, ..., numsr-1, numsr] is ascending if for all i where l <= i < r, numsi < numsi+1. Note that a subarray of size 1 is ascending.
=end

# @param {Integer[]} nums
# @return {Integer}
def max_ascending_sum(nums)
  max = []
  pointer = 0
  for i in 0..nums.size
    if nums[i+1] != nil
      if nums[i] >= nums[i+1]
        max << nums[pointer..i].sum
        pointer = i+1
      end
    else
      max << nums[pointer..i].sum
    end
  end       
  return max.max
end

puts max_ascending_sum([3,6,10,1,8,9,9,8,9])
