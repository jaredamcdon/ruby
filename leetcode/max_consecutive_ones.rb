=begin
Runtime: 84 ms, faster than 56.52% of Ruby online submissions
Memory Usage: 213 MB, less than 16.67% of Ruby online submissions
/---/
Given a binary array nums, return the maximum number of consecutive 1's in the array.
=end
# @param {Integer[]} nums
# @return {Integer}
def find_max_consecutive_ones(nums)
    counter = 0
    local = 0
    for i in 0..nums.size
        if nums[i] == 1
            local  += 1
        else
            if local > counter
                counter = local
            end
            local = 0
        end
    end
        return counter
end
