=begin
Runtime: 40 ms, faster than 100.00% of Ruby online submissions
Memory Usage: 209.6 MB, less than 93.02% of Ruby online submissions
###
Given a non-empty, singly linked list with head node head, return a middle node of linked list.

If there are two middle nodes, return the second middle node.
=end

# Definition for singly-linked list.
# class ListNode
#     attr_accessor :val, :next
#     def initialize(val = 0, _next = nil)
#         @val = val
#         @next = _next
#     end
# end
# @param {ListNode} head
# @return {ListNode}
def middle_node(head)
    list = []
    while head != nil
        list << head.val
        head = head.next
    end
    list = list[list.size/2..-1]
end
