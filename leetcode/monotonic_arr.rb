=begin
Runtime: 88 ms, faster than 61.11% of Ruby online submissions
Memory Usage: 215.1 MB, less than 72.22% of Ruby online submissions
/---/
An array is monotonic if it is either monotone increasing or monotone decreasing.
An array nums is monotone increasing if for all i <= j, nums[i] <= nums[j].  An array nums is monotone decreasing if for all i <= j, nums[i] >= nums[j].
Return true if and only if the given array nums is monotonic.
=end
# @param {Integer[]} nums
# @return {Boolean}
def is_monotonic(nums)
    if nums[0] < nums[-1]
        for i in 1...nums.size
            if nums[i] < nums[i-1]
                return false
            end
        end
    else
        for i in 1...nums.size
            if nums[i] > nums[i-1]
                return false
            end
        end
    end
    return true
end
