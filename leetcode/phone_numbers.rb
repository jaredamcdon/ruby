# @param {String} number
# @return {String}
def reformat_number(number)
    @number = number
    number= number.tr(' ','')
    number = number.tr('-', '')
    new_str = ""
    while number
        if number.size > 4
            new_str += "#{number[0..2]}-"
            number = number[3...number.size]
        elsif number.size == 4
            new_str+= "#{number[0..1]}-#{number[2..3]}"
            number = nil
        else
            new_str += number
            number = nil
        end
    end
    return new_str
end

=begin
62.5 percentile (not amazing)
50 percentile ram
=end
