=begin
Runtime: 76 ms, faster than 90.91% of Ruby online submissions
Memory Usage: 211.2 MB, less than 84.09% of Ruby online submissions
###/--/
Given an array arr, replace every element in that array with the greatest element among the elements to its right, and replace the last element with -1.

After doing so, return the array.
=end

# @param {Integer[]} arr
# @return {Integer[]}
def replace_elements(arr)
    if arr.size == 1
        return [-1]
    end
    arr = arr.reverse!
    new = [-1]
    new << arr[0]
    for i in 1...arr.size-1
        if arr[i] < arr[i-1]
            arr[i] = arr[i-1]
        end
        new << arr[i]
    end
    return new.reverse!
end
