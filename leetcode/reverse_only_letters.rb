=begin
Runtime: 56 ms, faster than 70.00% of Ruby online submissions
Memory Usage: 209.8 MB, less than 100.00% of Ruby online submissions
/---/
Given a string s, return the "reversed" string where all characters that are not a letter stay in the same place, and all letters reverse their positions.
=end
# @param {String} s
# @return {String}
def reverse_only_letters(s)
    cont = '-{}`<>.,@#$%^&*()+/[]_;:"!?=1234567890'
    cont+= "'"
    b = s.tr(cont,'')
    b = b.split('')
    for i in 0...s.size
        if cont.include? s[i] 
        else
            s[i] = b.pop()
        end
    end
    return s
end
