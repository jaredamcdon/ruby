=begin
Runtime: 56 ms, faster than 80.00% of Ruby online submissions
Memory Usage: 269.6 MB, less than 5.71% of Ruby online submissions
=end

# @param {String} s
# @return {String}
def reverse_words(s)
    @s = s
    split = s.split(' ')
    out = ""
    split.each { |i|out += "#{i.reverse!} " }
    out = out[0..-2]
    return out
end
