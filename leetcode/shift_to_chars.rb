=begin
Runtime: 52 ms, faster than 75.00% of Ruby online submissions
Memory Usage: 210.1 MB, less than 25.00% of Ruby online submissions
/----/
For every odd index i, you want to replace the digit s[i] with shift(s[i-1], s[i]).
Return s after replacing all digits. It is guaranteed that shift(s[i-1], s[i]) will never exceed 'z'.
=end
# @param {String} s
# @return {String}
def replace_digits(s)
    abet = [
        "a", "b", "c", "d",
        "e", "f", "g", "h",
        "i", "j", "k", "l",
        "m", "n", "o", "p",
        "q", "r", "s", "t",
        "u", "v", "w", "x",
        "y", "z"
    ]
    s = s.split('')
    out = ""
    for i in 0...s.size
        if abet.include? s[i]
            out += s[i]
        else
            out += abet[(abet.index s[i-1]) + s[i].to_i]
        end
    end
    return out
end
