=begin
Runtime: 112 ms, faster than 66.67% of Ruby online submissions
Memory Usage: 212.9 MB, less than 31.58% of Ruby online submissions
/---/
Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.
=end
# @param {Integer[]} nums
# @return {Integer[]}
def sorted_squares(nums)
    new =[]
    for i in nums
        new << i*i
    end
    return new.sort
end
